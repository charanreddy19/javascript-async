function fetchRandomNumbers(callback){
    console.log('Fetching number...');
    setTimeout(() => {
        let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
        console.log('Received random number:', randomNum);
        callback(randomNum);
    }, (Math.floor(Math.random() * (5)) + 1) * 1000);
}

function fetchRandomString(callback){
    console.log('Fetching string...');
    setTimeout(() => {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < 5; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        console.log('Received random string:', result);
        callback(result);
    }, (Math.floor(Math.random() * (5)) + 1) * 1000);
}



//fetchRandomNumbers((randomNum) => console.log(randomNum))
//fetchRandomString((randomStr) => console.log(randomStr))


//1

let fetchRandomNum=()=>{
    return new Promise((resolve,reject)=>{
    console.log('Fetching number...');
    setTimeout(() => {
        let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
        console.log('Received random number:', randomNum);
        resolve(randomNum)
    }, (Math.floor(Math.random() * (5)) + 1) * 1000)

  })
}

fetchRandomNum()
.then((randomnum)=>{
    console.log(randomnum)
})
.catch((err)=>{
    console.log(err)
})




let fetchRandomStr=()=>{
    return new Promise((resolve,reject)=>{
    console.log('Fetching string...');
    setTimeout(() => {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < 5; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        console.log('Received random string:', result);
        resolve(result)
    }, (Math.floor(Math.random() * (5)) + 1) * 1000);

  })
}

fetchRandomStr()
.then((str)=>{
    console.log(str)
})
.catch((err)=>{
    console.log(err)
})


//2

fetchRandomNum()
.then((num)=>{
    let sum=0
    sum += num
    console.log(sum)
    fetchRandomNum()
    .then((num1)=>{
        sum+=num1
        console.log('sum of two numbers is',sum)
    })
    .catch((err)=>{
        console.log(err)
    })
})
.catch((err)=>{
    console.log(err)
})


//3

Promise.all([
    fetchRandomNum(),fetchRandomStr()
])
.then((arr)=>{
    str=''
    for(let data of arr){
        str=str+data
    }
    console.log('sum of number and string is :',str)
    
})
.catch((err)=>{
    console.log(err)
})

//4

Promise.all([
    fetchRandomNum(),fetchRandomNum(),fetchRandomNum(),fetchRandomNum(),fetchRandomNum(),fetchRandomNum(),fetchRandomNum(),fetchRandomNum(),fetchRandomNum(),fetchRandomNum()
])
.then((arr)=>{
    let total=arr.reduce((sum,num)=>{
        return sum+num
    })
    console.log('10 numbers sum is',total)
})
.catch((err)=>{
    console.log(err)
})